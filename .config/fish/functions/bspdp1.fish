# Defined in - @ line 1
function bspdp1 --wraps='mv ~/.config/bspwm/bspwmrc ~/.config/bspwm/bspwmrc2 & mv ~/.config/bspwm/bspwmrc1 ~/.config/bspwm/bspwmrc & bspc wm -r & xrandr --auto' --description 'alias bspdp1 mv ~/.config/bspwm/bspwmrc ~/.config/bspwm/bspwmrc2 & mv ~/.config/bspwm/bspwmrc1 ~/.config/bspwm/bspwmrc & bspc wm -r & xrandr --auto'
  mv ~/.config/bspwm/bspwmrc ~/.config/bspwm/bspwmrc2 & mv ~/.config/bspwm/bspwmrc1 ~/.config/bspwm/bspwmrc & bspc wm -r & xrandr --auto $argv;
end
