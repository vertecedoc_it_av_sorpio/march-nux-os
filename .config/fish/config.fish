# ghcup-env
set PATH $PATH "$HOME/.cargo/bin" 
set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME
test -f /home/carlos/.ghcup/env ; and set -gx PATH $HOME/.cabal/bin /home/carlos/.ghcup/bin $PATH
starship init fish | source
